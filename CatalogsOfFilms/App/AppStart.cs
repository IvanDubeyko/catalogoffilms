﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Builder;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Security.OAuth;
using Data;
using Web.App;
using Common;

[assembly: PreApplicationStartMethod(typeof(AppStart), "Run")]

namespace Web.App
{
    public static class AppStart
    {
        public static void Run()
        {
            var builder = new ContainerBuilder();

            ConfigureBuilder(builder);

            builder.RegisterSource(new ViewRegistrationSource());

            var container = builder.Build();
            SetDependencyResolver(container);
        }

        public static void ConfigureBuilder(ContainerBuilder builder)
        {
            RegisterTypes(builder);

            builder.RegisterModule(new AutofacWebTypesModule());
            builder.Register(c => new RequestComponentContext(() => AutofacDependencyResolver.Current.RequestLifetimeScope)).As<IRequestComponentContext>().InstancePerRequest();
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterAssemblyTypes(Assembly.Load("Data"))
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<DataContext>().As<IDataContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterType<ApplicationOAuthProvider>().As<IOAuthAuthorizationServerProvider>().InstancePerRequest();

            builder.RegisterControllers(typeof(AppStart).Assembly).PropertiesAutowired();
        }

        private static void SetDependencyResolver(IContainer container)
        {
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            ServiceLocator.Register(serviceType => DependencyResolver.Current.GetService(serviceType));
        }
    }

    public static class RegistrationBuilderExtensions
    {
        public static void ApplyDefaultConfiguration(this IRegistrationBuilder<object, object, object> registrationBuilder, Func<IRegistrationBuilder<object, object, object>, IRegistrationBuilder<object, object, object>> lifetimeScopeConfigurator, bool dontAllowCircularDependencies = false)
        {
            lifetimeScopeConfigurator(registrationBuilder).PropertiesAutowired(dontAllowCircularDependencies ? PropertyWiringOptions.None : PropertyWiringOptions.AllowCircularDependencies);
        }

        public static void ApplyDefaultPerRequestConfiguration(this IRegistrationBuilder<object, object, object> registrationBuilder, bool dontAllowCircularDependencies = false)
        {
            registrationBuilder.InstancePerRequest().PropertiesAutowired(dontAllowCircularDependencies ? PropertyWiringOptions.None : PropertyWiringOptions.AllowCircularDependencies);
        }
    }
}