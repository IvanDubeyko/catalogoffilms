﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Autofac;
using Autofac.Integration.Owin;
using Data.Entities;
using Data.Repositories.Interfaces;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace Web.App
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userRepository = context.OwinContext.GetAutofacLifetimeScope().Resolve<IUserRepository>();

            User user;
            var isValid = userRepository.ValidateUser(context.UserName, context.Password, out user);
            if (!isValid)
            {
                context.SetError("invalid_grant", "The email or password is incorrect.");
                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            // create required claims
            identity.AddClaims(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
            });

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                { "userEmail", context.UserName }
            });

            var ticket = new AuthenticationTicket(identity, props);

            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            AddAuthenticationPropertiesToResponse(context);

            return base.TokenEndpoint(context);
        }

        private static void AddAuthenticationPropertiesToResponse(OAuthTokenEndpointContext context)
        {
            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
        }
    }
}