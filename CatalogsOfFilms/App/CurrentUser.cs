﻿using System;
using System.Security.Claims;
using System.Web;
using Common;
using Data.Entities;
using Data.Repositories.Interfaces;
using Web.Extensions;

namespace Web.App
{
    public static class CurrentUser
    {
        public const string UserKey = "CurrentUser";

        private static HttpContextBase HttpContext => ServiceLocator.Get<HttpContextBase>();

        public static AppUserState Get()
        {
            if (!HttpContext.Request.IsAuthenticated)
                return new AppUserState();

            var userIdentity = HttpContext.User.Identity as ClaimsIdentity;
            if (userIdentity == null)
                throw new InvalidOperationException(nameof(userIdentity));

            var user = (AppUserState)HttpContext.Items[UserKey];
            if (user == null)
            {
                var email = userIdentity.FindFirst(ClaimTypes.Email).Value;
                var userEntity = ServiceLocator.Get<IUserRepository>().GetUserByEmail(email);

                if (userEntity == null)
                    return new AppUserState();

                user = new AppUserState(userEntity);

                var currentUserClaim = userIdentity.FindFirst(UserKey);
                userIdentity.TryRemoveClaim(currentUserClaim);

                userIdentity.AddClaim(new Claim(UserKey, user.ToJson()));

                HttpContext.Items[UserKey] = user;
            }

            return user;
        }

        public static void SetToNull()
        {
            HttpContext.Items[UserKey] = null;
        }
    }

    public class AppUserState
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public DateTime CreatedAt { get; set; }

        public bool IsAnonymous() => Id == 0;

        public AppUserState()
        {
        }

        public AppUserState(User user)
        {
            Id = user.Id;
            Email = user.Email;
            CreatedAt = user.CreatedAt;
        }
    }
}