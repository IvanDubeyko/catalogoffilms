﻿using System;
using System.Collections;
using System.Web;
using Autofac;
using Web.Extensions;

namespace Web.App
{
    public interface IRequestComponentContext
    {
        object GetService(Type serviceType);
        T InjectProperties<T>(T target) where T : class;
        T InjectUnsetProperties<T>(T target) where T : class;
        void DisposeRequestLifetimeScope(IDictionary container, object key);
        void DisposeRequestLifetimeScope(HttpContextBase httpContext);
    }

    public class RequestComponentContext : IRequestComponentContext
    {
        private readonly Func<IComponentContext> _requestComponentContextProvider;

        public RequestComponentContext(Func<IComponentContext> requestComponentContextProvider)
        {
            _requestComponentContextProvider = requestComponentContextProvider;
        }

        public void DisposeRequestLifetimeScope(IDictionary container, object key)
        {
            if (container == null)
                throw new ArgumentNullException(nameof(container));

            if (key == null)
                throw new ArgumentNullException(nameof(key));

            if (!container.Contains(key))
                throw new InvalidOperationException("Failed to find any lifetime scope by key {0}.".FormatText(key));

            ((IDisposable)container[key]).Dispose();
            container[key] = null;
        }

        public void DisposeRequestLifetimeScope(HttpContextBase httpContext)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));

            DisposeRequestLifetimeScope(httpContext.Items, typeof(ILifetimeScope));
        }

        public object GetService(Type serviceType)
        {
            return _requestComponentContextProvider().Resolve(serviceType);
        }

        public T InjectProperties<T>(T target)
            where T : class
        {
            return _requestComponentContextProvider().InjectProperties(target);
        }

        public T InjectUnsetProperties<T>(T target)
            where T : class
        {
            return _requestComponentContextProvider().InjectUnsetProperties(target);
        }
    }
}
