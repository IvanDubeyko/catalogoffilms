﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Web.App.Startup))]

namespace Web.App
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            AppStart.ConfigureBuilder(builder);

            var container = builder.Build();

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            AuthConfig.ConfigureAuth(app);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
