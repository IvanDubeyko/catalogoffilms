﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Data;
using Web.App;

namespace Web
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            ConfigureBuilder(builder);

            builder.RegisterSource(new ViewRegistrationSource());

            var container = builder.Build();
            SetDependencyResolver(container);
        }

        private static void ConfigureBuilder(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(AppStart).Assembly);

            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterType<DataContext>().As<IDataContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterAssemblyTypes(Assembly.Load("Data")).Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces();

            builder.RegisterModule(new AutofacWebTypesModule());
        }

        private static void SetDependencyResolver(IContainer container)
        {
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}