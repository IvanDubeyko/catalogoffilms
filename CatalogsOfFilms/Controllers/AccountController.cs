﻿using System;
using System.Web.Mvc;
using Data;
using Data.Entities;
using Data.Repositories.Interfaces;
using Web.App;
using Web.Models.Account;
using Common;

namespace Web.Controllers
{
    public class AccountController : Controller
    {
        public IUserRepository UserRepository { get; set; }
        public IUnitOfWork UnitOfWork { get; set; }

        public ActionResult SignIn()
        {
            return View(new SignInModel());
        }

        [HttpPost]
        public ActionResult SignIn(SignInModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserRepository.GetUserByEmail(model.Email);

                if (UserRepository.ValidateUser(model.Email, model.Password, out user))
                {
                    IdentityHelper.IdentitySignin(new AppUserState(user));
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("Password", Resources.Account.InvalidData);
                return View(model);
            }
            return View(model);
        }

        public ActionResult SignOut()
        {
            IdentityHelper.IdentitySignout();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SignUp()
        {
            return View(new SignUpModel());
        }

        [HttpPost]
        public ActionResult SignUp(SignUpModel m)
        {
            if (ModelState.IsValid)
            {
                if (UserRepository.IsExistEmail(m.Email))
                {
                    ModelState.AddModelError("Email", Resources.Account.ExistEmail);
                    return View(m);
                }

                var user = UserRepository.Add(new User
                {
                    Email = m.Email,
                    PasswordHash = Cryptographer.ComputeSha512Hash(m.Password),
                    CreatedAt = DateTime.UtcNow
                });

                UnitOfWork.Commit();

                IdentityHelper.IdentitySignin(new AppUserState(user));

                return RedirectToAction("Index", "Home");
            }
            return View(m);
        }
    }
}