﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Common;
using Data;
using Data.Entities;
using Data.Repositories.Interfaces;
using Web.App;
using Web.Models.Film;

namespace Web.Controllers
{
    public class FilmController : Controller
    {
        public IUserRepository UserRepository { get; set; }
        public IFilmRepository FilmRepository { get; set; }
        public IUnitOfWork UnitOfWork { get; set; }

        public ActionResult AddFilm()
        {
            return View(new AddFilmModel());
        }

        [HttpPost]
        public ActionResult AddFilm(AddFilmModel m, HttpPostedFileBase poster)
        {
            if (ModelState.IsValid)
            {
                if (poster != null && poster.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(poster.InputStream))
                    {
                        var film = new Film
                        {
                            Name = m.Name,
                            Description = m.Description,
                            Producer = m.Producer,
                            Year = (int)m.Year,
                            Poster = reader.ReadBytes(poster.ContentLength),
                            UserId = CurrentUser.Get().Id,
                            CreatedAt = DateTime.UtcNow
                        };

                        var user = UserRepository.GetUserByEmail(CurrentUser.Get().Email);
                        if(user.Films == null)
                            user.Films = new List<Film> { film };
                        else
                            user.Films.Add(film);
                        FilmRepository.Add(film);
                        UnitOfWork.Commit();
                    }

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("poster", Resources.Film.FileNotChanged);
            }
            return View(m);
        }

        public ActionResult EditFilm(string eId)
        {
            var film = FilmRepository.GetById(Cryptographer.Decode(eId));
            var model = new EditFilmModel
            {
                Id = film.Id,
                Name = film.Name,
                Description = film.Description,
                Producer = film.Producer,
                Year = film.Year,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult EditFilm(EditFilmModel m, HttpPostedFileBase poster)
        {
            if (ModelState.IsValid)
            {
                var film = FilmRepository.GetById(m.Id);
                film.Name = m.Name;
                film.Description = m.Description;
                film.Year = (int)m.Year;
                film.Producer = m.Producer;

                if (poster != null && poster.ContentLength > 0)
                {
                    using (var reader = new System.IO.BinaryReader(poster.InputStream))
                    {
                        film.Poster = reader.ReadBytes(poster.ContentLength);
                    }
                }

                UnitOfWork.Commit();
                return RedirectToAction("Index", "Home");
            }
            return View(m);
        }

        public ActionResult Details(string eId)
        {
            var film = FilmRepository.GetById(Cryptographer.Decode(eId));

            var model = new DetailsModel
            {
                Id = film.Id,
                Name = film.Name,
                Description = film.Description,
                Year = film.Year,
                Producer = film.Producer,
                Poster = film.Poster,
                Author = UserRepository.GetById(film.UserId).Email
            };

            return View(model);
        }
    }
}