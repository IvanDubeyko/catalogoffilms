﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Data.Repositories.Interfaces;
using PagedList;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public IFilmRepository FilmRepository { get; set; }
        private const int DefaultPageSize = 6;

        public async Task<ActionResult> Index(int? page)
        {
            var pageNumber = page ?? 1;
            var films = await FilmRepository.GetAllAsync();

            return View(films.ToPagedList(pageNumber, DefaultPageSize));
        }
    }
}
