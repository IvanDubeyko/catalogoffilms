﻿using Newtonsoft.Json;

namespace Web.Extensions
{
    public static class StringExtensions
    {
        public static string ToJson(this object self)
        {
            if (self == null)
                return null;

            var result = JsonConvert.SerializeObject(self);

            return result;
        }

        public static string FormatText(this string self, params object[] args)
        {
            return string.Format(self, args);
        }
    }
}