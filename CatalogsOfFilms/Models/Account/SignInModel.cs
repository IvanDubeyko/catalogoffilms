﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.Account
{
    public class SignInModel
    {
        [Required]
        [StringLength(100, ErrorMessageResourceName = "ErrorEmail", ErrorMessageResourceType = typeof(Resources.Account))]
        [Display(Name = nameof(Resources.Account.Email), ResourceType = typeof(Resources.Account))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "ErrorPassword", ErrorMessageResourceType = typeof(Resources.Account), MinimumLength = 6)]
        [Display(Name = nameof(Resources.Account.Password), ResourceType = typeof(Resources.Account))]
        public string Password { get; set; }

        public bool Status { get; set; }

        public bool IsTwofAuth { get; set; }

        public string TwoFAuthError { get; set; }
    }
}