﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.Account
{
    public class SignUpModel
    {
        [Required]
        [StringLength(100, ErrorMessageResourceName = "ErrorEmail", ErrorMessageResourceType = typeof(Resources.Account))]
        [EmailAddress]
        [Display(Name = nameof(Resources.Account.Email), ResourceType = typeof(Resources.Account))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "ErrorPassword", ErrorMessageResourceType = typeof(Resources.Account), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = nameof(Resources.Account.Password), ResourceType = typeof(Resources.Account))]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceName = "ErrorConfirmPassword", ErrorMessageResourceType = typeof(Resources.Account))]
        [Display(Name = nameof(Resources.Account.ConfirmPassword), ResourceType = typeof(Resources.Account))]
        public string ConfirmPassword { get; set; }
    }
}