﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.Film
{
    public class AddFilmModel
    {
        [Required]
        [StringLength(100)]
        [Display(Name = nameof(Resources.Film.Name), ResourceType = typeof(Resources.Film))]
        public string Name { get; set; }

        [Required]
        [StringLength(1000)]
        [Display(Name = nameof(Resources.Film.Description), ResourceType = typeof(Resources.Film))]
        public string Description { get; set; }

        [Required]
        [Range(1900, 2100)]
        [Display(Name = nameof(Resources.Film.Year), ResourceType = typeof(Resources.Film))]
        public int? Year { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = nameof(Resources.Film.Producer), ResourceType = typeof(Resources.Film))]
        public string Producer { get; set; }
    }
}