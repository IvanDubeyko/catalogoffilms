﻿namespace Web.Models.Film
{
    public class DetailsModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Year { get; set; }

        public string Producer { get; set; }

        public string Author { get; set; }

        public byte[] Poster { get; set; }
    }
}