﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
    public static class Cryptographer
    {
        public static string ComputeSha512Hash(string valueToHash)
        {
            var algorithm = SHA512.Create();
            var hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(valueToHash));
            return Convert.ToBase64String(hash);
        }

        public static string Encode(int param)
        {
            var encoded = Encoding.UTF8.GetBytes(param.ToString());
            return Convert.ToBase64String(encoded);
        }

        public static int Decode(string param)
        {
            var encoded = Convert.FromBase64String(param);
            return Convert.ToInt32(Encoding.UTF8.GetString(encoded));
        }
    }
}