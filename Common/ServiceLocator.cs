﻿using System;

namespace Common
{
    public static class ServiceLocator
    {
        private static Func<Type, object> _globalProvider;

        public static void Register(Func<Type, object> provider)
        {
            _globalProvider = provider;
        }

        public static TService Get<TService>()
            where TService : class
        {
            return (TService)Get(typeof(TService));
        }

        public static object Get(Type serviceType)
        {
            return _globalProvider(serviceType);
        }
    }
}