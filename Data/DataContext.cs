﻿using System.Data.Entity;
using Data.Entities;

namespace Data
{
    public class DataContext : DbContext, IDataContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Film> Films { get; set; }

        IDbSet<TEntity> IDataContext.Set<TEntity>()
        {
            return Set<TEntity>();
        }

        public EntityState GetEntityState<TEntity>(TEntity entity) where TEntity : class
        {
            return Entry(entity).State;
        }

        public TEntity AttachEntity<TEntity>(TEntity entity, EntityState? withState = null) where TEntity : class
        {
            var attachedEntity = Set<TEntity>().Attach(entity);
            if (withState.HasValue)
                Entry(entity).State = withState.Value;

            return attachedEntity;
        }
    }
}
