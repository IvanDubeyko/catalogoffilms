﻿using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Film : BaseEntity
    {
        [Required, StringLength(255)]
        public string Name { get; set; }

        public string Description { get; set; }

        public int Year { get; set; }

        public string Producer { get; set; }

        public byte[] Poster { get; set; }

        public int UserId { get; set; }
    }
}
