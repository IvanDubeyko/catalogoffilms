﻿using System;
using System.Data.Entity;
using Data.Entities;

namespace Data
{
    public interface IDataContext : IDisposable
    {
        DbSet<User> Users { get; set; }
        DbSet<Film> Films { get; set; }

        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityState GetEntityState<TEntity>(TEntity entity) where TEntity : class;
        TEntity AttachEntity<TEntity>(TEntity entity, EntityState? withState = null) where TEntity : class;

        int SaveChanges();
    }
}
