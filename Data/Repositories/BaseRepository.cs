﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Data.Entities;
using Data.Repositories.Interfaces;

namespace Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDataContext _dataContext;
        private readonly IDbSet<TEntity> _dbset;

        protected BaseRepository(IDataContext dataContext)
        {
            _dataContext = dataContext;
            _dbset = dataContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return _dbset.AsNoTracking();
        }

        public virtual IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> predicate)
        {
            return GetAll().Where(predicate);
        }

        public virtual async Task<IList<TEntity>> GetAllAsync()
        {
            return await GetAll().ToListAsync();
        }

        public virtual async Task<IList<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await GetAll().Where(predicate).ToListAsync();
        }

        public virtual TEntity GetBy(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbset.SingleOrDefault(predicate);
        }

        public virtual TEntity GetById(int id)
        {
            return _dbset.Find(id);
        }

        public virtual TEntity Add(TEntity entity)
        {
            return _dbset.Add(entity);
        }

        public virtual void Edit(TEntity entity)
        {
            var entityState = _dataContext.GetEntityState(entity);
            if (entityState == EntityState.Detached)
            {
                entityState = EntityState.Modified;
                _dataContext.AttachEntity(entity, entityState);
            }
        }

        public virtual TEntity Delete(TEntity entity)
        {
            return _dbset.Remove(entity);
        }

        public virtual void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}
