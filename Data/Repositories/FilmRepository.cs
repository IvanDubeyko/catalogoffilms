﻿using Data.Entities;
using Data.Repositories.Interfaces;

namespace Data.Repositories
{
    public class FilmRepository: BaseRepository<Film>, IFilmRepository
    {
        public FilmRepository(IDataContext dataContext) : base(dataContext)
        {
        }
    }
}
