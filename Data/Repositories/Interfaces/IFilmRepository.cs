﻿using Data.Entities;

namespace Data.Repositories.Interfaces
{
    public interface IFilmRepository : IBaseRepository<Film>
    {
    }
}
