﻿using Data.Entities;

namespace Data.Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User GetUserByEmail(string email);
        bool ValidateUser(string email, string password, out User user);
        bool IsExistEmail(string email);
    }
}
