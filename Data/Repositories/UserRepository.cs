﻿using Common;
using Data.Entities;
using Data.Repositories.Interfaces;

namespace Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IDataContext dataContext) : base(dataContext)
        {
        }

        public User GetUserByEmail(string email)
        {
            return GetBy(u => u.Email == email);
        }

        public bool ValidateUser(string email, string password, out User user)
        {
            user = GetUserByEmail(email);

            return user != null && user.PasswordHash == Cryptographer.ComputeSha512Hash(password);
        }

        public bool IsExistEmail(string email)
        {
            return GetUserByEmail(email) != null;
        }
    }
}
